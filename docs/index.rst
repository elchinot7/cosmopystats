.. cosmopystats documentation master file, created by
   sphinx-quickstart on Sun Apr  3 01:24:56 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

CosmoPyStats's documentation
========================================

.. figure:: _static/triangle_SN_1_test1.png
   :height: 200px
   :width: 400 px
   :scale: 100 %
   :alt: emcee triangle
   :figclass: align-center

   Triangle plot produced by ``emcee + corner``. In this example ``corner`` color scheme was modified.

Contents:

.. toctree::
   :maxdepth: 2

   Readme
   Classes
   Examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

