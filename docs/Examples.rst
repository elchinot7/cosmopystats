MPI Example
===========


.. automodule:: LCDM_emcee_MPI
   :members:
   :undoc-members:
   :inherited-members:
   :show-inheritance:
