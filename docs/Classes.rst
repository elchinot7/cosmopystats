Cosmology Classes
=================

.. autoclass:: cosmomodel.cosmomodel.CosmoModel
  :members:
  :undoc-members:
  :inherited-members:
  :show-inheritance:
  :special-members: __init__

.. autoclass:: cosmomodel.cosmomodel.LCDM_Flat
  :members:
  :undoc-members:
  :show-inheritance:
  :special-members: __init__
