########################
CosmoPyStats
########################

:Author:  Efrain Torres
:Date:    2016-01-16

Readme.rst
========================

Bayesian Statistics
===================

Using bayesian analysis ``CosmoPyStats`` can produce triangle plots: 

.. figure:: _static/triangle_SN_1_test1.png
   :height: 200px
   :width: 400 px
   :scale: 100 %
   :alt: emcee triangle
   :figclass: align-center

   Triangle plot produced by ``emcee + corner``. In this example ``corner`` was modiefied. 



emcee implementation
--------------------

``CosmoPyStats`` uses ``emcee``, which is a generic bayesian module that can be installed using ``pip``:

.. code:: sh

   pip install emcee
   pip install corner


Hubble Diagram
==============

``CosmoPyStats`` has dedicated functions to plot the Hubble diagram
using the Supernova data.

.. figure:: _static/SuperNova_luminosity_1.png
   :height: 200px
   :width: 400 px
   :scale: 100 %
   :alt: Hubble Diagram
   :figclass: align-center

   Data shown was extracted from SN Union2.1 last release.

Frequentist Statistics
======================

Chi squared analysis
--------------------

.. figure:: _static/SN_Union_Conf_Interv.png
   :height: 200px
   :width: 400 px
   :scale: 100 %
   :alt: Frequentist minimization 
   :figclass: align-center

   Regions with :math:`{\chi^2}_\nu = cte` were obtained using scipy minimization subroutines. 




History: Cloning Arturo (Mathematica)
-------------------------------------

This Python package was exercise was inspired by Avelino's `Mathematica` code produced during his Phd,  but subsequently was modified to follow the scheme
previously published by Scalpy_ (See [arxiv:1503.02407])

Cosmo Info
----------


Dependencies
------------

Some python dependencies are required:

1. numpy_
2. matplotlib_
3. emcee_

versions
--------

``v0.2.1``
~~~~~~~~~~

``v0.2``
~~~~~~~~

``v0.1``
~~~~~~~~~


.. [arxiv:1503.02407] http://arxiv.org/abs/1503.02407)
.. _Scalpy: https://github.com/sum33it/scalpy
.. _numpy: http://www.numpy.org
.. _matplotlib: http://matplotlib.org
.. _emcee: http://dan.iel.fm/emcee/current/


