SHELL := /bin/bash
#-----------------------------------------------
#         Makefile for CosmoPyStats
#-----------------------------------------------
#	Author:      Efrain Torres
#	Email:       efraazu@gmail.com
#	Github:      https://github.com/elchinot7
#	Description: This can be used to
#	            - Install
#	            - Reinstall
#	            - Run
#	            python package and scripts
#-----------------------------------------------

#  --------------------------------------
#  Apps needed are different for Linux/Mac
#  --------------------------------------
ifeq ($(shell uname -s),Darwin)
    OPEN = open
	PYTHON = python
else
    OPEN = xdg-open
	PYTHON = python
endif
#  --------------------------------------

.PHONY: docs install reinstall

# --------------------------------------
#INSTALL_OPT = install
INSTALL_OPT = develop
INSTALL_DEST = ~/Programs/cosmopystats
# --------------------------------------

default: plot

install: setup.py
	python setup.py $(INSTALL_OPT) --prefix=$(INSTALL_DEST)

# ---------------------------------------------------------------------------
# This is the simplest way to install packages into https://cloud.sagemath.com
# This will intall inside
# ~/.local/lib/python2.7/site-packages
# See:
#     https://groups.google.com/forum/#!topic/sage-cloud/5ReEM8DSx1Q
# ---------------------------------------------------------------------------
sage-install: setup.py
	sage --python setup.py install --user

reinstall: setup.py
	python setup.py $(INSTALL_OPT) --prefix=$(INSTALL_DEST)

docs:
	cd docs && make clean && make html
	$(OPEN) docs/_build/html/index.html

# -----------------------------
# Some styles and colors to be
# used in Terminal outputs
# -----------------------------
REDC = \033[31m
BOLD = \033[1m
GREENC = \033[32m
UNDERLINE = \033[4m
ENDC = \033[0m
# -----------------------------

# --------------------------------------------------------------
help:
	@echo "------------------------------------------------------"
	@echo -e "                    $(BOLD)$(UNDERLINE)$(REDC)< CosmoPyStats >$(ENDC)"
	@echo -e "                     $(GREENC)Makefile Menu$(ENDC)"
	@echo "------------------------------------------------------"
	@echo "Please use 'make <target>' where target is one of:"
	@echo
	@echo -e "$(BOLD)$(REDC)default$(ENDC)   $(REDC)docs$(ENDC)"
	@echo -e "$(REDC)install$(ENDC)    > Install package"
	@echo
	@echo -e "$(REDC)reinstall$(ENDC)  > Reinstall package"
	@echo
	@echo -e "$(REDC)docs$(ENDC)       > Generate & shows documentation"
	@echo
	@echo -e "$(REDC)help$(ENDC)       > Show this menu"
	@echo "------------------------------------------------------"

# --------------------------------------------------------------
