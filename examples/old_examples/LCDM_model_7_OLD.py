'''
Author: Efrain Torres
Date: Jan 20 2016
'''

import numpy as np
import os
import matplotlib.pyplot as plt
from myutils.myUtils import *
from cosmomodel.cosmoModel import *
from cosmomodel.MCMC import MCMC
from cosmomodel.chi2minimizer import Chi2Minimizer


data_dir = os.path.join('./data')
data_file = 'SN_Union2_1.dat'
data_covmat_file = 'SN_Union2_1_covmat.dat'
plot_dest = os.path.join('./plots')
plot_name = 'SuperNova.png'

font = {'family': 'Times New Roman',
        'weight': 'normal',  # light, bold
        'size': 15}
plt.rc('font', **font)  # change general fonts


def plotHubbleDiagram(ax, dataSN):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    redShift = dataSN['RedShift']
    distance = dataSN['Distance']
    distanceErr = dataSN['DistanceErr']

    ax.errorbar(redShift, distance, yerr=distanceErr, fmt='o', ms=3)

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu(z)$')


def plotmu(ax, model, zarray):
    """docstring for plotmu"""
    muarray = []
    for z in zarray:
        muarray.append(model.mu(z))
    ax.plot(zarray, muarray, label=model.label)


def load_SNdata(pathName, fileName, verbose=True):
    SNData = np.loadtxt(os.path.join(data_dir, data_file),
                        dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
                               'formats': ('|S15', np.float, np.float, np.float, np.float)},
                        skiprows=5)
    if verbose is True:
        printBar('-')
        print 'Number of SuperNova Loaded :{}'.format(len(SNData))
        printBar('-')
    return SNData


def load_SNcovmat(pathName, fileName, verbose=True):
    covmat = np.loadtxt(os.path.join(data_dir, data_covmat_file))
    if verbose is True:
        printBar('-')
        print 'covmat data info:\nCovariance Matrix is a ({0} ,{1}) matrix'.format(len(covmat), len(covmat[0]))
        printBar('-')
    return covmat


if __name__ == "__main__":

    def main():
        """docstring for main"""

        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)

        H0 = 71.0
        zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = load_SNdata(data_dir, data_file)
        plotHubbleDiagram(ax1, UnionSN21Data)

        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))

        # self.omegas = [OmegaM, OmegaR, OmegaL, OmegaK]
        model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=H0)
        model2 = cosmoModel(omegas=[0.2, 0.0, 0.0, 0.8], H0=H0)
        model3 = cosmoModel(omegas=[1.0, 0.0, 0.0, 0.0], H0=H0)
        model4 = cosmoModel(omegas=[0.50, 0.0, 1.0 - 0.50 - 0.0, 0.0], H0=H0)

        models = [model1, model2, model3, model4]

        for model in models:
            print model
            plotmu(ax1, model, zarray)

        plt.legend(loc='lower right')

        # plt.savefig(os.path.join(plot_dest, plot_name), dpi=90, bbox_inches='tight')
        plt.show()

    def testchisqrd():
        """docstring for testchisqrd"""
        # fig1 = plt.figure()
        # ax1 = fig1.add_subplot(111)

        H0 = 71.0
        # zarray = np.linspace(0.01, 1.7, 50)

        UnionSN21Data = load_SNdata(data_dir, data_file)
        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))
        # plotHubbleDiagram(ax1, UnionSN21Data)

        model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=H0)
        # plt.legend(loc='lower right')
        # plt.show()
        print model1
        print model1.SNChiSqrd(UnionSN21Data)

    def testMCMC():
        """docstring for testFitter"""
        H0 = 71.0
        # zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = load_SNdata(data_dir, data_file)
        print 'Number of SuperNova Loaded :{}'.format(len(UnionSN21Data))
        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_covmat_nosys.txt
        UnionSN21_covmat = load_SNcovmat(data_dir, data_covmat_file)
        # print 'covmat data info:\nCovariance Matrix is a ({0} ,{1})\nmatrix'.format(len(UnionSN21_covmat), len(UnionSN21_covmat[0]))

        # plotHubbleDiagram(ax1, UnionSN21Data)

        model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=H0)
        # plt.legend(loc='lower right')
        # plt.show()
        print model1
        # print model1.SNChiSqrd(UnionSN21Data)
        # fakemodel = 'fakemodel'
        # model1.getMCMC(UnionSN21Data, UnionSN21Data_covmat)
        mcmc = MCMC(model1, UnionSN21Data, UnionSN21_covmat)
        # mcmc = MCMC(fakemodel)
        # mcmc()
        print mcmc.model.OmegaR
        print mcmc
        print mcmc.chi2SN()
        print mcmc.lnlike()
        print mcmc.lnprob()
        mcmc.getGuessForWalkers()
        print model1

    def testChiMinimizer():
        """docstring for testChiMinimizer"""
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        SN21_data = load_SNdata(data_dir, data_file, verbose=BE_VERBOSE)
        SN21_covmat = load_SNcovmat(data_dir, data_covmat_file, verbose=BE_VERBOSE)

        # model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=71.0)
        model1 = LCDM_Flat(OmegaM=0.25, H0=71.0)
        # model1 = LCDM_Flat()
        fitmodel = Chi2Minimizer(model1, SN21_data, SN21_covmat)

        print fitmodel
        print '\nTesting...\nchi2()={}\n'.format(fitmodel.chi2())
        fitmodel.getBestFit(method='Nelder-Mead')
        print fitmodel.bestFit
        print fitmodel.model.OmegaM
        fitmodel.marginalizeH0()
        print fitmodel.model.FreePars
        # fitmodel.getBestFit(toMinimize='marginalizeH0_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)

    def testChiMinimizerNew():
        """docstring for testChiMinimizerNew"""
        # ---------------------------------------------
        BE_VERBOSE = False
        # ---------------------------------------------
        SN21_data = load_SNdata(data_dir, data_file, verbose=BE_VERBOSE)
        SN21_covmat = load_SNcovmat(data_dir, data_covmat_file, verbose=BE_VERBOSE)

        model1 = LCDM_Flat_New(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        fitmodel = Chi2Minimizer(model1, SN21_data, SN21_covmat)
        print fitmodel

        printBar()
        print '\nTesting...\nmodel.mu()={}\n'.format(fitmodel.model.mu(1))
        printBar()
        print '\nTesting...\nchi2()={}\n'.format(fitmodel.chi2())
        printBar()
        print "\n Testing...\nfitmodel.getBestFit(method='Nelder-Mead')"
        fitmodel.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead')
        print "\n Testing...\n Now the fitmodel.bestfit is defined: {}".format(fitmodel.bestFit)
        printBar()
        print "Testing...You can call a model parameter value with:\nfitmodel.model.pars['OmegaM'][0]  # DO NOT use this here! "
        printBar()
        print "Testing...print fitmodel.model.pars['OmegaL'][0] to get {}".format(fitmodel.model.pars['OmegaL'][0])
        printBar()

        # fitmodel.marginalizeH0()  # DO NOT use this here!
        # print fitmodel.model.FreePars
        printBar()
        print "Testing ... Marginalize over H1 and then get the Best Fit with\n\
            fitmodel.getBestFit(toMinimize='marginalizeH0_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)\n\
            Marginalizing, BE PATIENT!!....\n"
        fitmodel.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        printBar()
        print "Testing... Now the fitmodel.bestfit is defined as:".format(fitmodel.bestFit)
        printBar()

    # -------------------------------
    # Select jus one function to run
    # -------------------------------
    # main()  # Plot Union 2.1 SN & three models examples
    # -------------------------------
    # testchisqrd()  # Test the SNChiSqrd() function
    # -------------------------------
    # Timing(testchisqrd)  # Same as testchisqrd() but get the total run-time.
    # -------------------------------
    # Timing(testMCMC)  # Bayesian analysis
    # -------------------------------
    # Timing(testChiMinimizer, 'testChiMinimizer')  #
    Timing(testChiMinimizerNew, 'testChiMinimizerNew')  #
    # -------------------------------
