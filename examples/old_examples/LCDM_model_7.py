'''
Author: Efrain Torres
Date: Jan 20 2016
'''

import numpy as np
import os
import matplotlib.pyplot as plt
from utils.utils import *
from cosmomodel.cosmoModel import *
from stats.mcmc import MCMC
from stats.chi2minimizer import Chi2Minimizer
from utils.dataLoaders import load_SNdata, load_SNcovmat


# data_dir = os.path.join('./data')
# data_file = 'SN_Union2_1.dat'
# data_covmat_file = 'SN_Union2_1_covmat.dat'
plot_dest = os.path.join('./plots')
plot_name = 'SuperNova.png'

font = {'family': 'Times New Roman',
        'weight': 'normal',  # light, bold
        'size': 15}
plt.rc('font', **font)  # change general fonts


def plotHubbleDiagram(ax, dataSN):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    redShift = dataSN['RedShift']
    distance = dataSN['Distance']
    distanceErr = dataSN['DistanceErr']

    ax.errorbar(redShift, distance, yerr=distanceErr, fmt='o', ms=3)

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu(z)$')


def plotmu(ax, model, zarray):
    """docstring for plotmu"""
    muarray = []
    for z in zarray:
        muarray.append(model.mu(z))
    ax.plot(zarray, muarray, label=model.plotLabel)


# def load_SNdata(pathName, fileName, verbose=True):
#     SNData = np.loadtxt(os.path.join(data_dir, data_file),
#                         dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
#                                'formats': ('|S15', np.float, np.float, np.float, np.float)},
#                         skiprows=5)
#     if verbose is True:
#         printBar('-')
#         print 'Number of SuperNova Loaded :{}'.format(len(SNData))
#         printBar('-')
#     return SNData


# def load_SNcovmat(pathName, fileName, verbose=True):
#     covmat = np.loadtxt(os.path.join(data_dir, data_covmat_file))
#     if verbose is True:
#         printBar('-')
#         print 'covmat data info:\nCovariance Matrix is a ({0} ,{1}) matrix'.format(len(covmat), len(covmat[0]))
#         printBar('-')
#     return covmat


if __name__ == "__main__":

    def main():
        """docstring for main"""

        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)

        H0 = 71.0
        zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = load_SNdata(file_name='./data/SN_Union2_1.dat')
        plotHubbleDiagram(ax1, UnionSN21Data)

        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))

        model1 = cosmoModel(pars={"OmegaM": [0.27, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [1.0 - 0.27, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        model2 = cosmoModel(pars={"OmegaM": [0.2, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [0.0, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        model3 = cosmoModel(pars={"OmegaM": [1.0, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [0.0, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        model4 = cosmoModel(pars={"OmegaM": [0.5, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [0.5, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})

        # model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=H0)
        # model2 = cosmoModel(omegas=[0.2, 0.0, 0.0, 0.8], H0=H0)
        # model3 = cosmoModel(omegas=[1.0, 0.0, 0.0, 0.0], H0=H0)
        # model4 = cosmoModel(omegas=[0.50, 0.0, 1.0 - 0.50 - 0.0, 0.0], H0=H0)

        models = [model1, model2, model3, model4]
        # models = [model1]

        for model in models:
            print model
            plotmu(ax1, model, zarray)

        plt.legend(loc='lower right')

        plot_name = 'SuperNova_luminosity.png'
        plt.savefig(os.path.join(plot_dest, plot_name), dpi=90, bbox_inches='tight')
        plt.show()

    def testMCMC():
        """docstring for testFitter"""
        # H0 = 71.0
        # zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = load_SNdata(file_name='./data/SN_Union2_1.dat')
        print 'Number of SuperNova Loaded :{}'.format(len(UnionSN21Data))
        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_covmat_nosys.txt
        UnionSN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat')
        # print 'covmat data info:\nCovariance Matrix is a ({0} ,{1})\nmatrix'.format(len(UnionSN21_covmat), len(UnionSN21_covmat[0]))

        # plotHubbleDiagram(ax1, UnionSN21Data)

        # model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0], H0=H0)
        model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        # plt.legend(loc='lower right')
        # plt.show()
        print model1
        # print model1.SNChiSqrd(UnionSN21Data)
        # fakemodel = 'fakemodel'
        # model1.getMCMC(UnionSN21Data, UnionSN21Data_covmat)
        mcmc = MCMC(model1, UnionSN21Data, UnionSN21_covmat)
        # mcmc = MCMC(fakemodel)
        # mcmc()
        print mcmc.model.value['OmegaM']
        print mcmc
        print mcmc.chi2SN()
        print mcmc.lnlike()
        print mcmc.lnprob()
        mcmc.getGuessForWalkers()
        print model1

    def testChiMinimizer():
        """docstring for testChiMinimizerNew"""
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
        SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)

        model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        fitmodel = Chi2Minimizer(model1, SN21_data, SN21_covmat)
        print fitmodel

        printBar()
        print '\nTesting...\nmodel.mu()={}\n'.format(fitmodel.model.mu(1))
        printBar()
        print '\nTesting...\nchi2()={}\n'.format(fitmodel.chi2())
        printBar()
        print "\n Testing...\nfitmodel.getBestFit(method='Nelder-Mead')"
        fitmodel.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        print "\n Testing...\n Now the fitmodel.bestfit is defined: {}".format(fitmodel.bestFit)
        printBar()
        print "Testing...You can call a model parameter value with:\nfitmodel.model.value['OmegaM'] "
        printBar()
        print "Testing...print fitmodel.model.value['OmegaL'] to get {}".format(fitmodel.model.value['OmegaL'])
        printBar()

        # fitmodel.marginalizeH0()  # DO NOT use this here!
        # print fitmodel.model.freePars
        printBar()
        print "Testing ... Marginalize over H1 and then get the Best Fit with\n\
            fitmodel.getBestFit(toMinimize='marginalizeH0_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)\n\
            Marginalizing, BE PATIENT!!....\n"
        fitmodel.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        printBar()
        print "Testing... Now the fitmodel.bestfit is defined as: {}".format(fitmodel.bestFit)
        printBar()

    def plotChiMinimizer():
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
        SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)

        # model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        model2 = cosmoModel(pars={"OmegaM": [0.3, 0.2, 0.5, True],
                                  "OmegaR": [0.00005, 0.0, 0.0, False],
                                  "OmegaL": [0.6, 0.5, 1.0, True],
                                  "H0": [63.0, 60.0, 80.0, True]})
        # fitmodel1 = Chi2Minimizer(model1, SN21_data, SN21_covmat)
        fitmodel2 = Chi2Minimizer(model2, SN21_data, SN21_covmat)

        # fitmodel2.marginalizeH0()  # DO NOT use this here!
        # print fitmodel2.model.freePars

        # fitmodel1.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        fitmodel2.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)

        # fitmodel1.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        # fitmodel2.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)

        ax = fitmodel2.getPlot()
        ax.set_title('SN Union Constraints')
        ax.set_xlabel(r'$\Omega_M$')
        ax.set_ylabel(r'$\Omega_\Lambda$')
        plt.savefig('plots/SN_Union_Conf_Interv.png')

    def testMCMC_NEW():
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
        SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)

        # model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        model2 = cosmoModel(pars={"OmegaM": [0.3, 0.2, 0.5, True],
                                  "OmegaR": [0.00005, 0.0, 0.0, False],
                                  "OmegaL": [0.6, 0.5, 1.0, True],
                                  "H0": [63.0, 60.0, 80.0, True]})

        mcmc = MCMC_NEW(model2, SN21_data, SN21_covmat,
                        version='emcee', nWalkers=6,
                        chainSteps=100, burnin=5)

        mcmc.Run()

    # -------------------------------
    # Select jus one function to run
    # -------------------------------
    # main()  # Plot Union 2.1 SN & three models examples
    # -------------------------------
    # Timing(testMCMC)  # Bayesian analysis
    # -------------------------------
    # Timing(testChiMinimizer, 'testChiMinimizer')  #
    # -------------------------------
    Timing(plotChiMinimizer, 'plotChiMinimizer')  #
    # -------------------------------
    # Timing(testMCMC_NEW)  # Bayesian analysis
    # -------------------------------
