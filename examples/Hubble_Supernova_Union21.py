'''
Author: Efrain Torres
Date: Jan 20 2016
'''

from cosmopystats.cosmomodels import CosmoModel
from cosmopystats.data.dataLoaders import get_unionSN21Data
from cosmopystats.utils import setup_text_plots, print_sign
import numpy as np
import os
import matplotlib.pyplot as plt
from itertools import cycle
from matplotlib import gridspec


plot_dest = os.path.join('./plots')
setup_text_plots(fontsize=15, family='Times New Roman', style='normal',
                 weight='normal', usetex=True)

# font = {'family': 'Times New Roman',
#         'weight': 'normal',  # light, bold
#         'size': 15}
# plt.rc('font', **font)  # change general fonts


def plotHubbleDiagram(ax, dataSN):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    ax.errorbar(dataSN['RedShift'], dataSN['Distance'], yerr=dataSN['DistanceErr'],
                fmt='o', ms=3, mfc='0.7', ecolor='0.5')

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu(z)$')


def plotHubbleDiagram_rescaled(ax, dataSN, model_rescaler):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    distance_res = [dataSN['Distance'][i] - model_rescaler.mu(dataSN['RedShift'][i]) for i in xrange(len(dataSN['RedShift']))]

    ax.errorbar(dataSN['RedShift'], distance_res, yerr=dataSN['DistanceErr'], fmt='o',
                ms=3, mfc='0.7', ecolor='0.5')

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu-M$')


def main():
    """docstring for main"""
    PLOTS_VERSION = 'BW'

    if PLOTS_VERSION is 'color':
        lines = ["r--", "b-", "m-", "g-", "k-", "y-", "c-",
                 "r--", "b--", "m--", "g--", "k--", "y--", "c--"]
        line_width = 1.3
    elif PLOTS_VERSION is 'BW':
        lines = ["k-", "k--", "k-.", "k:"]
        line_width = 1

    linecycler = cycle(lines)
    gs = gridspec.GridSpec(2, 1, height_ratios=[2, 1])
    fig = plt.figure()
    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1])
    plt.subplots_adjust(hspace=.001)

    H0 = 72.0
    zarray = np.linspace(0.01, 1.5, 50)

    # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
    # dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    dataSN = get_unionSN21Data()

    print 'Number of SuperNova:{}'.format(len(dataSN))

    model1 = CosmoModel(pars={"OmegaM": [0.27, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [1.0 - 0.27, 0.0, 1.0, False],
                              "H0": [67.3, 50.0, 80.0, False]})
    model2 = CosmoModel(pars={"OmegaM": [0.2, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [0.0, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})
    model3 = CosmoModel(pars={"OmegaM": [1.0, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [0.0, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})

    models = [model1, model2, model3]

    # plotHubbleDiagram
    ax1.errorbar(dataSN['RedShift'], dataSN['Distance'], yerr=dataSN['DistanceErr'],
                 fmt='o', ms=3, mfc='0.7', ecolor='0.5')

    ax1.set_xlabel(r'$z$')
    ax1.set_ylabel(r'$\mu(z)$')

    # plotHubbleDiagram_rescaled
    distance_res = [dataSN['Distance'][i] - model2.mu(dataSN['RedShift'][i]) for i in xrange(len(dataSN['RedShift']))]

    ax2.errorbar(dataSN['RedShift'], distance_res, yerr=dataSN['DistanceErr'], fmt='o',
                 ms=3, mfc='0.7', ecolor='0.5')

    ax2.set_xlabel(r'$z$')
    ax2.set_ylabel(r'$\mu-M$')

    for model in models:
        print model
        ls = next(linecycler)

        mu = [model.mu(z) for z in zarray]
        mu_rescaled = [(model.mu(z)-model2.mu(z)) for z in zarray]

        ax1.plot(zarray, mu, ls, linewidth=line_width, label=model.plotLabel)
        ax2.plot(zarray, mu_rescaled, ls, linewidth=line_width)

    ax1.legend(loc='lower right', fancybox=True)
    ax1.set_xlabel('')
    ax1.set_xlim(0, 1.6)
    ax1.set_ylim(33, 46)
    plt.setp(ax1.get_xticklabels(), visible=False)

    ax2.set_yticks(np.arange(-2, 3, 1))
    ax2.set_xlim(0, 1.6)
    ax2.set_ylim(-2, 2)

    print_sign(fig)
    plot_name = 'SuperNova_luminosity_2.pdf'
    plt.savefig(os.path.join(plot_dest, plot_name), dpi=90, bbox_inches='tight')
    plt.show()

if __name__ == "__main__":
    # -------------------------------
    main()  # Plot Union 2.1 SN & three models examples
    # -------------------------------
