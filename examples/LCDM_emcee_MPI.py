'''
Document this
Author: Efrain
Date: Feb 3 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import sys
import numpy as np
from utils.utils import Timing, setup_text_plots, printBar, progressbar
from utils.dataLoaders import load_SNdata, load_SNcovmat
from cosmomodel.cosmomodel import CosmoModel
from stats.mcmc import MCMC
from emcee.utils import MPIPool
import emcee

#  weight: 'normal',  'light', 'bold'
setup_text_plots(fontsize=20, family='serif', style='normal',
                 weight='bold', usetex=True)


def run_mcmc(sampler, pos0, iterations, storechain=True,
             rstate0=None, savechain=True, printstep=1):
    printBar(mark='-')
    print("Running emcee")
    printBar(mark='-')
    print("BE PATIENT & GET SOME BEER...")
    printBar(mark='-')
    # print '\n'
    for i, result in enumerate(sampler.sample(pos0, iterations=iterations,
                                              storechain=storechain, rstate0=rstate0)):
        if savechain is True:
            position = result[0]
            f = open("chains/chain.dat", "a")
            for k in range(position.shape[0]):
                f.write("{0:4d} {1:s}\n".format(k, " ".join(str(position[k]))))
            f.close()
        if (i+1) % printstep == 0:
            advance = 100 * float(i+1) / iterations
            progressbar(advance)
    print ' Done!'


def lnp(theta, mcmc):
    '''
    This is a trick used to avoid the mpi4py error origined by the requirement:
       -> What can be pickled and unpickled?
          built-in functions defined at the top level of a module
    http://stackoverflow.com/questions/8804830/python-multiprocessing-pickling-error
    '''
    return mcmc.lnp(theta)


def emcee_test():
    # ---------------------------------------------
    BE_VERBOSE = True
    N_WALKERS = 50  # 6, 50
    CHAINS_STEPS = 1000  # 50, 1000
    BURN = 100  # 2, 100
    COLOR_DENSE_2D = 'b'
    USE_MPI = True
    SAVE_CHAINS = False
    PRINT_STEP = 10  # print advance every (PRINT_STEP) %
    COLOR_DENSE_2D = 'b'
    USE_MPI = True
    SAVE_CHAINS = False
    PRINT_STEP = 1  # print advance every (PRINT_STEP) %
    # ---------------------------------------------
    # DATA
    # ---------------------------------------------
    SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
    SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)
    # ---------------------------------------------
    # Cosmological model
    # ---------------------------------------------
    model = CosmoModel(pars={"OmegaM": [0.3, 0.2, 0.5, True],
                             "OmegaR": [0.00005, 0.0, 0.0, False],
                             "OmegaL": [0.6, 0.5, 1.0, True],
                             "H0": [63.0, 60.0, 80.0, True]})
    # ---------------------------------------------
    # Stats tools
    # ---------------------------------------------
    mcmc = MCMC(model=model, data=SN21_data, covmat=SN21_covmat,
                version='emcee', nWalkers=N_WALKERS,
                chainSteps=CHAINS_STEPS, burnin=BURN,
                color=COLOR_DENSE_2D, verbose=BE_VERBOSE)
    # ---------------------------------------------

    # mcmc.Run_and_Plot()
    # chains = mcmc.getMCMC()
    if USE_MPI is True:
        pool = MPIPool()
        if not pool.is_master():
            pool.wait()
            sys.exit(0)

        pos0 = mcmc.getGuessForWalkers()
        sampler = emcee.EnsembleSampler(mcmc.nWalkers, mcmc.Ndims, lnp, args=(mcmc,), pool=pool)
    else:
        pos0 = mcmc.getGuessForWalkers()
        sampler = emcee.EnsembleSampler(mcmc.nWalkers, mcmc.Ndims, mcmc.lnp)

    # Run 100 steps as a burn-in.
    pos1, prob, state = sampler.run_mcmc(pos0, BURN)

    # Reset the chain to remove the burn-in samples.
    sampler.reset()

    if SAVE_CHAINS is True:
        f = open("chains/chain.dat", "w")
        f.close()

    # sampler.run_mcmc(pos1, mcmc.chainSteps, rstate0=state)
    for i, result in enumerate(sampler.sample(pos1, iterations=mcmc.chainSteps,
                                              storechain=True, rstate0=state)):
        if SAVE_CHAINS is True:
            position = result[0]
            f = open("chains/chain.dat", "a")
            for k in range(position.shape[0]):
                f.write("{0:4d} {1:s}\n".format(k, " ".join(str(position[k]))))
            f.close()
        if (i+1) % PRINT_STEP == 0:
            progressbar(advance=i+1, total=mcmc.chainSteps)

    if USE_MPI is True:
        pool.close()
    mcmc.plotMCMC(sampler, save_plots=True)
    # plt.show()
    # ---------------------------------------------
    # Print out the mean acceptance fraction. In general, acceptance_fraction
    # has an entry for each walker so, in this case, it is a 250-dimensional
    # vector.
    print(u"\nMean acceptance fraction: ", np.mean(sampler.acceptance_fraction))

    # Compute the evidence.
    # API notes at http://dan.iel.fm/emcee/current/api/#the-parallel-tempered-ensemble-sampler

    # approximation, uncertainty = sampler.thermodynamic_integration_log_evidence()

    # Report!

    mcmc.plotMCMC(sampler, save_plots=True, show_plot=True)
    mcmc.report(sampler)

if __name__ == "__main__":
    # -------------------------------
    # Select just one function to run
    # -------------------------------
    Timing(emcee_test)  # Timing runs emcee_test() and show the Run Time
    # -------------------------------
