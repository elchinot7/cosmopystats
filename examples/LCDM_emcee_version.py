'''
Document this
Author: Efrain
Date: Feb 3 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
from cosmopystats.cosmomodels import CosmoModel
from cosmopystats.stats.mcmc import MCMC
from cosmopystats.utils import Timing, setup_text_plots, progressbar
from cosmopystats.data.dataLoaders import get_unionSN21Data, get_unionSN21Covmat
from cosmopystats.utils.dataPlotters import plotHubbleDiagram, plotmu
import matplotlib.pyplot as plt
import numpy as np
import emcee
import os

#  weight: 'normal',  'light', 'bold'
setup_text_plots(fontsize=20, family='serif', style='normal', weight='bold', usetex=True)


def hubble_test():
    """docstring for main"""
    # ---------------------------------------------
    # BE_VERBOSE = True
    SAVE_HUBBLE_PLOT = True
    PLOT_NAME = 'Hubble_Union21.png'
    PLOT_DEST = os.path.join('./plots')
    # ---------------------------------------------
    # ---------------------------------------------
    fig1 = plt.figure()
    ax1 = fig1.add_subplot(111)
    H0 = 71.0
    zarray = np.linspace(0.01, 1.7, 50)
    # ---------------------------------------------
    # DATA
    # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
    # UnionSN21Data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
    UnionSN21Data = get_unionSN21Data()
    # ---------------------------------------------
    plotHubbleDiagram(ax1, UnionSN21Data)
    # ---------------------------------------------

    model1 = CosmoModel(pars={"OmegaM": [0.27, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [1.0 - 0.27, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})
    model2 = CosmoModel(pars={"OmegaM": [0.2, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [0.0, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})
    model3 = CosmoModel(pars={"OmegaM": [1.0, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [0.0, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})
    model4 = CosmoModel(pars={"OmegaM": [0.5, 0.0, 1.0, True],
                              "OmegaR": [0.0, 0.0, 0.0, False],
                              "OmegaL": [0.5, 0.0, 1.0, False],
                              "H0": [H0, 50.0, 80.0, False]})
    models = [model1, model2, model3, model4]
    for model in models:
        print model
        plotmu(ax=ax1, model=model, zarray=zarray)
    plt.legend(loc='lower right')

    if SAVE_HUBBLE_PLOT is True:
        plt.savefig(os.path.join(PLOT_DEST, PLOT_NAME),
                    dpi=90, bbox_inches='tight')
    plt.show()


def lnp(theta, mcmc):
    """docstring for lnp"""
    return mcmc.lnp(theta)


def emcee_test():
    # ---------------------------------------------
    BE_VERBOSE = True
    N_WALKERS = 6  # 6, 50
    CHAINS_STEPS = 50  # 50, 1000
    BURN = 2  # 100
    COLOR_DENSE_2D = 'b'
    # USE_MPI = False
    SAVE_CHAINS = False
    PRINT_STEP = 1  # print advance every (PRINT_STEP) %
    # ---------------------------------------------
    # ---------------------------------------------
    # DATA
    # ---------------------------------------------
    # SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
    # SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)
    SN21_data = get_unionSN21Data()
    SN21_covmat = get_unionSN21Covmat()
    # ---------------------------------------------
    model = CosmoModel(pars={"OmegaM": [0.3, 0.2, 0.5, True],
                             "OmegaR": [0.00005, 0.0, 0.0, False],
                             "OmegaL": [0.6, 0.5, 1.0, True],
                             "H0": [63.0, 60.0, 80.0, True]})
    # ---------------------------------------------
    mcmc = MCMC(model=model, data=SN21_data, covmat=SN21_covmat,
                version='emcee', nWalkers=N_WALKERS,
                chainSteps=CHAINS_STEPS, burnin=BURN,
                color=COLOR_DENSE_2D, MPI=False, verbose=BE_VERBOSE)
    # ---------------------------------------------

    # mcmc.Run_and_Plot()
    # chains = mcmc.getMCMC()
    pos0 = mcmc.getGuessForWalkers()
    sampler = emcee.EnsembleSampler(mcmc.nWalkers, mcmc.Ndims, mcmc.lnp)

    # Run 100 steps as a burn-in.
    pos1, prob, state = sampler.run_mcmc(pos0, BURN)

    # Reset the chain to remove the burn-in samples.
    sampler.reset()

    # sampler.run_mcmc(pos1, mcmc.chainSteps, rstate0=state)
    for i, result in enumerate(sampler.sample(pos1, iterations=mcmc.chainSteps,
                                              storechain=True, rstate0=state)):
        if SAVE_CHAINS is True:
            position = result[0]
            f = open("chains/chain_single.dat", "a")
            for k in range(position.shape[0]):
                f.write("{0:4d} {1:s}\n".format(k, " ".join(str(position[k]))))
            f.close()
        if (i+1) % PRINT_STEP == 0:
            progressbar(advance=i+1, total=mcmc.chainSteps)

    mcmc.plotMCMC(sampler, save_plots=True)
    plt.show()
    mcmc.report(sampler)

if __name__ == "__main__":
    # -------------------------------
    # Select just one function to run
    # -------------------------------
    # hubble_test()  # Plot Union 2.1 SN & three models examples
    # -------------------------------
    # emcee_test()  # Bayes analysis using emcee
    # -------------------------------
    Timing(emcee_test)  # Timing runs emcee_test() and show the Run Time
    # -------------------------------
