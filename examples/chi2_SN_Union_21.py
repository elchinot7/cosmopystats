'''
Author: Efrain Torres
Date: Jan 20 2016
'''

from cosmopystats.cosmomodels import CosmoModel, LCDM_Flat
from cosmopystats.data.dataLoaders import get_unionSN21Data, get_unionSN21Covmat
import numpy as np
import os
import matplotlib.pyplot as plt
# from utils.utils import *
from cosmopystats.utils import printBar, Timing
# from cosmomodel.cosmomodel import *
# from stats.mcmc import MCMC
# from stats.chi2minimizer import Chi2Minimizer
from cosmopystats.stats.chi2minimizer import Chi2Minimizer
# from utils.dataLoaders import load_SNdata, load_SNcovmat
from itertools import cycle
from matplotlib import gridspec


plot_dest = os.path.join('./plots')
plot_name = 'SuperNova.png'

font = {'family': 'Times New Roman',
        'weight': 'normal',  # light, bold
        'size': 15}
plt.rc('font', **font)  # change general fonts


def plotHubbleDiagram(ax, dataSN):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    ax.errorbar(dataSN['RedShift'], dataSN['Distance'], yerr=dataSN['DistanceErr'],
                fmt='o', ms=3, mfc='0.7', ecolor='0.5')

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu(z)$')


def plotHubbleDiagram_rescaled(ax, dataSN, model_rescaler):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    distance_res = [dataSN['Distance'][i] - model_rescaler.mu(dataSN['RedShift'][i]) for i in xrange(len(dataSN['RedShift']))]

    ax.errorbar(dataSN['RedShift'], distance_res, yerr=dataSN['DistanceErr'], fmt='o',
                ms=3, mfc='0.7', ecolor='0.5')

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu-M$')


if __name__ == "__main__":

    def main():
        """docstring for main"""
        PLOTS_VERSION = 'BW'
        if PLOTS_VERSION is 'color':
            lines = ["r--", "b-", "m-", "g-", "k-", "y-", "c-",
                     "r--", "b--", "m--", "g--", "k--", "y--", "c--"]
            line_width = 1.3
        elif PLOTS_VERSION is 'BW':
            lines = ["k-", "k--", "k-.", "k:"]
            line_width = 1

        linecycler = cycle(lines)
        gs = gridspec.GridSpec(2, 1, height_ratios=[2, 1])
        fig = plt.figure()
        ax1 = fig.add_subplot(gs[0])
        ax2 = fig.add_subplot(gs[1])
        plt.subplots_adjust(hspace=.001)

        H0 = 72.0
        zarray = np.linspace(0.01, 1.5, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        # UnionSN21Data = load_SNdata(file_name='./data/SN_Union2_1.dat')
        UnionSN21Data = get_unionSN21Data()

        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))

        model1 = CosmoModel(pars={"OmegaM": [0.27, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [1.0 - 0.27, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        model2 = CosmoModel(pars={"OmegaM": [0.2, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [0.0, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        model3 = CosmoModel(pars={"OmegaM": [1.0, 0.0, 1.0, True],
                                  "OmegaR": [0.0, 0.0, 0.0, False],
                                  "OmegaL": [0.0, 0.0, 1.0, False],
                                  "H0": [H0, 50.0, 80.0, False]})
        # model4 = CosmoModel(pars={"OmegaM": [0.5, 0.0, 1.0, True],
        #                           "OmegaR": [0.0, 0.0, 0.0, False],
        #                           "OmegaL": [0.5, 0.0, 1.0, False],
        #                           "H0": [H0, 50.0, 80.0, False]})

        # models = [model1, model2, model3, model4]
        models = [model1, model2, model3]

        plotHubbleDiagram(ax1, UnionSN21Data)
        plotHubbleDiagram_rescaled(ax2, UnionSN21Data, model2)

        for model in models:
            print model
            ls = next(linecycler)

            mu = [model.mu(z) for z in zarray]
            mu_rescaled = [(model.mu(z)-model2.mu(z)) for z in zarray]

            ax1.plot(zarray, mu, ls, linewidth=line_width, label=model.plotLabel)
            ax2.plot(zarray, mu_rescaled, ls, linewidth=line_width)

        ax1.legend(loc='lower right', fancybox=True)
        ax1.set_xlabel('')
        ax1.set_xlim(0, 1.6)
        ax1.set_ylim(33, 46)
        plt.setp(ax1.get_xticklabels(), visible=False)

        # ax2 = fig.add_subplot(212)
        # plotHubbleDiagram_rescaled(ax2, UnionSN21Data, model2)
        ax2.set_yticks(np.arange(-2, 3, 1))
        ax2.set_xlim(0, 1.6)
        ax2.set_ylim(-2, 2)

        plot_name = 'SuperNova_luminosity_1.pdf'
        plt.savefig(os.path.join(plot_dest, plot_name), dpi=90, bbox_inches='tight')
        plt.show()

    def testChiMinimizer():
        """docstring for testChiMinimizerNew"""
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        # SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
        # SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)
        SN21_data = get_unionSN21Data()
        SN21_covmat = get_unionSN21Covmat()

        model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        fitmodel = Chi2Minimizer(model1, SN21_data, SN21_covmat)
        print fitmodel

        printBar()
        print '\nTesting...\nmodel.mu()={}\n'.format(fitmodel.model.mu(1))
        printBar()
        print '\nTesting...\nchi2()={}\n'.format(fitmodel.chi2())
        printBar()
        print "\n Testing...\nfitmodel.getBestFit(method='Nelder-Mead')"
        fitmodel.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        print "\n Testing...\n Now the fitmodel.bestfit is defined: {}".format(fitmodel.bestFit)
        printBar()
        print "Testing...You can call a model parameter value with:\nfitmodel.model.value['OmegaM'] "
        printBar()
        print "Testing...print fitmodel.model.value['OmegaL'] to get {}".format(fitmodel.model.value['OmegaL'])
        printBar()

        # fitmodel.marginalizeH0()  # DO NOT use this here!
        # print fitmodel.model.freePars
        printBar()
        print "Testing ... Marginalize over H1 and then get the Best Fit with\n\
            fitmodel.getBestFit(toMinimize='marginalizeH0_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)\n\
            Marginalizing, BE PATIENT!!....\n"
        fitmodel.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        printBar()
        print "Testing... Now the fitmodel.bestfit is defined as: {}".format(fitmodel.bestFit)
        printBar()

    def plotChiMinimizer():
        # ---------------------------------------------
        BE_VERBOSE = True
        # ---------------------------------------------
        # SN21_data = load_SNdata(file_name='./data/SN_Union2_1.dat', verbose=BE_VERBOSE)
        # SN21_covmat = load_SNcovmat(file_name='./data/SN_Union2_1_covmat.dat', verbose=BE_VERBOSE)
        SN21_data = get_unionSN21Data()
        SN21_covmat = get_unionSN21Covmat()

        # model1 = LCDM_Flat(OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 40.0, 80.0])
        model2 = CosmoModel(pars={"OmegaM": [0.3, 0.2, 0.5, True],
                                  "OmegaR": [0.00005, 0.0, 0.0, False],
                                  "OmegaL": [0.6, 0.5, 1.0, True],
                                  "H0": [70.0, 60.0, 80.0, True]})
        # fitmodel1 = Chi2Minimizer(model1, SN21_data, SN21_covmat)
        fitmodel2 = Chi2Minimizer(model2, SN21_data, SN21_covmat)

        # fitmodel2.marginalizeH0()  # DO NOT use this here!
        # print fitmodel2.model.freePars

        # fitmodel1.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        fitmodel2.getBestFit(ToDo='minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)

        # fitmodel1.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)
        # fitmodel2.getBestFit(ToDo='first_marginalize_H0_then_minimize_Chi2', method='Nelder-Mead', verbose=BE_VERBOSE)

        ax = fitmodel2.getPlot()
        ax.set_title('SN Union Constraints')
        ax.set_xlabel(r'$\Omega_M$')
        ax.set_ylabel(r'$\Omega_\Lambda$')
        plt.savefig('plots/SN_Union_Conf_Interv.png')

    # -------------------------------
    # Select jus one function to run
    # -------------------------------
    # main()  # Plot Union 2.1 SN & three models examples
    # -------------------------------
    # Timing(testMCMC)  # Bayesian analysis
    # -------------------------------
    # Timing(testChiMinimizer, 'testChiMinimizer')  #
    # -------------------------------
    Timing(plotChiMinimizer, 'plotChiMinimizer')  #
    # -------------------------------
    # Timing(testMCMC_NEW)  # Bayesian analysis
    # -------------------------------
