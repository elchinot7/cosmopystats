'''
Document this
Author: Efrain
date: Jan 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import numpy as np
import sys
import scipy.optimize as opt
# from utils.utils import printBar
import matplotlib.pyplot as plt
import emcee
import corner
from emcee.utils import MPIPool


def printBar(mark='*', nbars=1, size=30):
    """docstring for printBar"""
    for x in xrange(0, nbars):
        print mark[:6]*size


class MCMC(object):
    '''
    Based in :
        https://github.com/sum33it/scalpy/blob/9b2d714017e78731069a1c8c0c210f2bcd659116/example/emcee_chain.py
        Versions defined:
            1) 'emcee'
            2) 'pymc'  NOT YET !
    ToDo:
        0) Include and test the MPI capabilities, how can I modified this class to produce thread-safe independent models?
           Check this for more info: http://stackoverflow.com/questions/8804830/python-multiprocessing-pickling-error
        1) The appearence of things like:
               - mu_obs = self.data['Distance']
               - self.model.value['OmegaM'] = O_m
            prevents the use of this Class in a completely generic problem.
        2) It would be great if capabilities to save and reload MCMC chains can be included
        3) A function able to produce a report with the best fit, sigma 1-2-3 limits is necessary.
    '''
    def __init__(self, model, data, covmat=None, version='naive',
                 Ndims=2, nWalkers=100, chainSteps=1000, burnin=100,
                 color='k', MPI=False, verbose=False):
        '''Document this __init__'''
        # if MPI is True:
        #     sys.exit('\nMPI version not implemented yet, sorry!\n')
        self.model = model
        self.data = data
        if covmat is not None:
            self.covmat = covmat
        self.version = version
        # self.marginCount = 0
        if version is 'emcee':
            self.Ndims = Ndims
            self.nWalkers = nWalkers
            self.chainSteps = chainSteps
            self.burnin = burnin
            self.MPI = MPI
        # plots
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.color = color
        # To be obtained later
        self.bestFit = None
        self.bestChi2 = None
        self.confidenceOneSigma = None
        self.confidenceTwoSigma = None
        self.confidenceThreeSigma = None
        # Flag to display less/more output
        self.verbose = verbose

    def __str__(self):
        "Displays some specific output"
        return self.out_info()

    def __call__(self, *args, **kwargs):
        "Displays some specific output"
        for d in sorted(dir(self)):
            print "{}: {}". format(d, str(getattr(self, d)))

    def out_info(self):
        out = '\n=================================\n'
        out += 'This is a {} instance:\n'.format(self.__class__.__name__)
        out += 'Model Info:{}'.format(self.model.out_info())
        out += 'Data Info:\n{} Supenova\n'.format(len(self.data))
        if self.version is 'emcee':
            out += 'Ndims: {}\nnWalkers: {}\nmodel: {}\nfitter: {}'\
                .format(self.Ndims, self.nWalkers, self.model.__class__.__name__, self.version)
        out += '=================================\n'
        return out

    def updateConfidenceLevels(self):
        self.confidenceOneSigma = self.bestChi2 + 2.3
        self.confidenceTwoSigma = self.bestChi2 + 6.17
        self.confidenceThreeSigma = self.bestChi2 + 11.8

    def chi2SN_covmat(self):
        # ld = np.vectorize(cosmoModel(pars=pars).lum_dis_z)
        # x = 5.0*np.log10(ld(zsn))+25.-mu_b  #  what is this "- mu_b" ?
        zData = self.SNData['RedShift']
        # muData = self.SNData['Distance']
        # muError = self.SNData['DistanceErr']
        x = []
        for z in zData:
            x.append(self.model.mu(z))
        covinv = np.linalg.inv(self.covmat)
        # return covinv
        return np.dot(x, np.dot(covinv, x))

    def chi2(self):
        """docstring for chi2
        ----------------------------------
        See eq (A4) Phys Rev D72, 123519
        ----------------------------------
        """
        mu_obs = self.data['Distance']
        z_obs = self.data['RedShift']
        mu_err = self.data['DistanceErr']
        A = 0.
        B = 0.
        C = 0.
        for z, m, err in zip(z_obs, mu_obs, mu_err):
            diff = m - self.model.mu(z)
            A += diff**2 / err**2
            B += diff / err**2
            C += 1.0 / err**2
        return A - B**2 / C

    # def chi2_MPI(self, model):
    #     """docstring for chi2
    #     ----------------------------------
    #     See eq (A4) Phys Rev D72, 123519
    #     ----------------------------------
    #     """
    #     mu_obs = self.data['Distance']
    #     z_obs = self.data['RedShift']
    #     mu_err = self.data['DistanceErr']
    #     A = 0.
    #     B = 0.
    #     C = 0.
    #     for z, m, err in zip(z_obs, mu_obs, mu_err):
    #         diff = m - model.mu(z)
    #         A += diff**2 / err**2
    #         B += diff / err**2
    #         C += 1.0 / err**2
    #     return A - B**2 / C

    def lnprior(self):
        # But what if there are more free parameters?
        if self.model.lower['OmegaM'] < self.model.value['OmegaM'] < self.model.upper['OmegaM']:
            return 0.0
        if self.model.lower['OmegaL'] < self.model.value['OmegaL'] < self.model.upper['OmegaL']:
            return 0.0
        return -np.inf

    # def lnprior_MPI(self, model):
    #     # But what if there are more free parameters?
    #     if model.lower['OmegaM'] < model.value['OmegaM'] < model.upper['OmegaM']:
    #         return 0.0
    #     if model.lower['OmegaL'] < model.value['OmegaL'] < model.upper['OmegaL']:
    #         return 0.0
    #     return -np.inf

    def lnlike(self):
        return -self.chi2()/2.0

    # def lnlike_MPI(self, model):
    #     return -self.chi2(model=model)/2.0

    def lnprob(self):
        lp = self.lnprior()
        if not np.isfinite(lp):
            return -np.inf
        return lp + self.lnlike()

    # def lnprob_MPI(self, model):
    #     lp = self.lnprior_MPI(model=model)
    #     if not np.isfinite(lp):
    #         return -np.inf
    #     # return lp + self.lnlike_MPI(model=model)
    #     return 1.0

    def lnp(self, theta):
        '''First update the model
           then get the Ln Prob'''
        O_m, O_l = theta
        self.model.value['OmegaM'] = O_m
        # self.model.value['OmegaL'] = 1.0 - O_m
        self.model.value['OmegaL'] = O_l
        return self.lnprob()

    # def lnp_MPI(self, theta):
    #     '''First update the model
    #        then get the Ln Prob'''
    #     model_tmp = self.model

    #     O_m, O_l = theta
    #     model_tmp.value['OmegaM'] = O_m
    #     # model_tmp.value['OmegaL'] = 1.0 - O_m
    #     model_tmp.value['OmegaL'] = O_l
    #     return self.lnprob_MPI(model=model_tmp)

    def updateChi2(self, theta):
        '''Used by opt-minimize chi2'''
        self.model.value['OmegaM'] = theta[0]
        # self.model.value['OmegaL'] = 1.0 - theta[0]
        self.model.value['OmegaL'] = theta[1]
        return self.chi2()

    def getBestFit(self, ToDo='minimize_with_scipy', method='Nelder-Mead'):
        '''
        ToDo:
            'minimize_Chi2',
            'first_marginalize_H0_then_minimize_Chi2' NOT INCLUDED HERE
        Methods:
            - Powell,
            - CG,
            - Newton-CG
        '''
        thetaGuess = []
        for parname, parval in self.model.freePars.iteritems():
            if parname != 'H0':
                thetaGuess.append(parval)
                if self.verbose is True:
                    print "Par {} is added to thetaGuess".format(parname)
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        # CHOOSE WHAT ToDo:
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        if ToDo is 'minimize_with_scipy':
            print 'Getting best initial guess for walkers (scipy minimization)...'
            result = opt.minimize(self.updateChi2, thetaGuess, method=method)
        else:
            sys.exit('Error: "ToDo" not implemented in getBestFit()')
        # - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
        self.bestFit = result.x
        self.bestChi2 = result.fun
        if self.version is 'naive':
            self.updateConfidenceLevels()
            self.ax.plot([self.bestFit[0]], [self.bestFit[1]], 'ro')

        if self.verbose is True:
            printBar(mark='<>', nbars=1, size=25)
            print 'Scipy best fit report:'
            printBar(mark='<>', nbars=1, size=25)
            print result
            printBar(mark='--', nbars=1, size=25)
            print 'Maximum likelihood result: {}'.format(result.fun)
            print 'Best Fit = {0} (guess: {1})'.format(self.bestFit, thetaGuess)
            printBar(mark='<>', nbars=2, size=25)
            print ''

    def getGuessForWalkers(self):
        # Set up the sampler.
        self.getBestFit()
        pos0 = [self.bestFit + 1e-4*np.random.randn(self.Ndims) for i in range(self.nWalkers)]
        return pos0

    def getMCMC(self):
        '''
        Based in:
        https://github.com/sum33it/scalpy/blob/9b2d714017e78731069a1c8c0c210f2bcd659116/example/emcee_chain.py
        Versions defined:
            1) 'emcee'
            2) 'pymc'  NOT YET !
        '''
        if self.version is 'emcee':
            # Find the maximum likelihood value.
            # &
            # Set up the sampler.
            # pos0 = self.getGuessForWalkers()

            if self.MPI is True:
                pool = MPIPool()
                if not pool.is_master():
                    pool.wait()
                    sys.exit(0)
                pos0 = self.getGuessForWalkers()
                sampler = emcee.EnsembleSampler(self.nWalkers, self.Ndims, self.lnp_MPI, pool=pool)
                # sampler = emcee.EnsembleSampler(self.nWalkers, self.Ndims, self.lnp, pool=pool)
            else:
                pos0 = self.getGuessForWalkers()
                sampler = emcee.EnsembleSampler(self.nWalkers, self.Ndims, self.lnp)

            # Clear and run the production chain.
            printBar(mark='-')
            print("Running emcee")
            printBar()
            print("BE PATIENT & GET SOME BEER...")
            printBar()
            sampler.run_mcmc(pos0, self.chainSteps)

            if self.MPI is True:
                pool.close()

            print("Chains done!")
            printBar(mark='-')
            return sampler

    def plotMCMC(self, sampler, save_plots=True, show_plot=False):
        ''' Plot:
            1)line time walkers evolution
            2)triangle plots
        '''
        # ---------------------------------------------------------
        printBar()
        print 'Plotting MCMC chains...'
        printBar()
        plt.clf()
        fig, axes = plt.subplots(self.Ndims, 1, sharex=True, figsize=(8, 9))
        alpha_line_time = 0.4
        color_line_time = 'k'
        denseColor = self.color
        # ---------------------------------------------------------
        axes[0].plot(sampler.chain[:, :, 0].T, color=color_line_time, alpha=alpha_line_time)
        # axes[0].yaxis.set_major_locator(MaxNLocator(5))
        axes[0].axhline(self.bestFit[0], color="r", lw=2)
        axes[0].set_ylabel(self.model.label['OmegaM'])
        # ---------------------------------------------------------
        axes[1].plot(sampler.chain[:, :, 1].T, color=color_line_time, alpha=alpha_line_time)
        # axes[1].yaxis.set_major_locator(MaxNLocator(5))
        axes[1].axhline(self.bestFit[1], color="r", lw=2)
        axes[1].set_ylabel(self.model.label['OmegaL'])
        # ---------------------------------------------------------
        fig.tight_layout(h_pad=0.0)
        if save_plots is True:
            fig.savefig("emcee_plots/line_time_SN.png")
        # ---------------------------------------------------------
        # Make the triangle plot.
        # ---------------------------------------------------------
        samples = sampler.chain[:, self.burnin:, :].reshape((-1, self.Ndims))
        fig = corner.corner(samples, labels=[self.model.label['OmegaM'],
                                             self.model.label['OmegaL']],
                            plot_datapoints=False, color=denseColor, fill_contours=True)
        # ---------------------------------------------------------
        if save_plots is True:
            fig.savefig("emcee_plots/triangle_SN.png")

        if show_plot is True:
            plt.show()

    def report(self, sampler):
        # ---------------------------------------------
        # Print out the mean acceptance fraction. In general, acceptance_fraction
        # has an entry for each walker so, in this case, it is a 250-dimensional
        # vector.
        print "\nMean acceptance fraction: {} ".format(np.mean(sampler.acceptance_fraction))

        # Compute the evidence.
        # API notes at http://dan.iel.fm/emcee/current/api/#the-parallel-tempered-ensemble-sampler

        # approximation, uncertainty = sampler.thermodynamic_integration_log_evidence()

        # Report!

        # print 'Estimated log evidence = ,{} +/- {}'.format(approximation, uncertainty)

    def Run(self):
        return self.getMCMC()

    def Run_and_Plot(self):
        sampler1 = self.getMCMC()
        print 'Plotting:'
        self.plotMCMC(sampler1)
        plt.show()
