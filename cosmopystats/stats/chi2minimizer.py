'''
Document this
Author: Efrain
date: Jan 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import numpy as np
import sys
import scipy.optimize as opt
# from myutils.myUtils import *
from scipy.integrate import quad
# from scipy.optimize import fsolve
import matplotlib.pyplot as plt
import matplotlib.mlab as ml
from pylab import cm
from matplotlib.lines import Line2D


def printBar(mark='*', nbars=1, size=30):
    """docstring for printBar"""
    for x in xrange(0, nbars):
        print mark[:6]*size


class Chi2Minimizer(object):
    '''
    Based in :
    Write documentation ...
    '''
    def __init__(self, model, data, covmat, version='naive'):
        """docstring for __init__"""
        self.model = model
        self.data = data
        self.covmat = covmat
        self.version = version
        self.bestFit = None
        self.bestChi2 = None
        self.confidenceOneSigma = None
        self.confidenceTwoSigma = None
        self.confidenceThreeSigma = None
        self.fig = plt.figure()
        self.ax = self.fig.add_subplot(111)
        self.marginCount = 0

    def __str__(self):
        "Displays some specific output"
        return self.out_info()

    def __call__(self, *args, **kwargs):
        "Displays some specific output"
        for d in sorted(dir(self)):
            print "{}: {}". format(d, str(getattr(self, d)))

    def out_info(self):
        out = '\n=================================\n'
        out += 'This is a {} instance:\n'.format(self.__class__.__name__)
        out += 'Model Info:{}'.format(self.model.out_info())
        out += 'Data Info:\n{} Supenova\n'.format(len(self.data))
        out += '=================================\n'
        return out

    def updateConfidenceLevels(self):
        self.confidenceOneSigma = self.bestChi2 + 2.3
        self.confidenceTwoSigma = self.bestChi2 + 6.17
        self.confidenceThreeSigma = self.bestChi2 + 11.8

    def chi2(self):
        """docstring for chi2
        ----------------------------------
        See eq (A4) Phys Rev D72, 123519
        ----------------------------------
        """
        mu_obs = self.data['Distance']
        z_obs = self.data['RedShift']
        mu_err = self.data['DistanceErr']
        A = 0.
        B = 0.
        C = 0.
        for z, m, err in zip(z_obs, mu_obs, mu_err):
            diff = m - self.model.mu(z)
            A += diff**2 / err**2
            B += diff / err**2
            C += 1.0 / err**2
        return A - B**2 / C

    def updateChi2(self, theta):
        '''Used by opt-minimize chi2'''
        self.model.value['OmegaM'] = theta[0]
        # self.model.value['OmegaL'] = 1.0 - theta[0]
        self.model.value['OmegaL'] = theta[1]
        return self.chi2()

    def updateChi2_new(self, theta0, theta1):
        '''Used by opt-minimize chi2'''
        self.model.value['OmegaM'] = theta0
        # self.model.value['OmegaL'] = 1.0 - theta[0]
        self.model.value['OmegaL'] = theta1
        chi2 = self.chi2()
        return chi2

    def pdfChi2(self):
        return np.exp(-self.chi2()/2.0)

    def updatePdfChi2(self, H0):
        '''Used by quad-integrate PDFchi2'''
        self.model.H0 = H0
        return self.pdfChi2()

    def marginalizeH0(self):
        I = quad(self.updatePdfChi2, self.model.lower['H0'], self.model.upper['H0'])
        marg = -2.0*np.log(I[0])
        return marg

    def updateMarginalizeH0(self, theta):
        self.model.value['OmegaM'] = theta[0]
        # self.model.value['OmegaL'] = 1.0 - theta[0]
        self.model.value['OmegaL'] = theta[1]
        return self.marginalizeH0()

    def getBestFit(self, ToDo='minimize_Chi2', method='Nelder-Mead', verbose=True):
        '''
        ToDo:
            'minimize_Chi2',
            'first_marginalize_H0_then_minimize_Chi2'
        Methods:
            - Powell,
            - CG,
            - Newton-CG
        '''

        thetaGuess = []
        print "\tWARNING FIX THIS, I am in <chi2minimizer()> under getBestFit()"

        for parname, parval in self.model.freePars.iteritems():
            # if parname != 'H0':
            if parname != 'H00':
                thetaGuess.append(parval)
                printBar(mark='-')
                print "\tPar {} is added to thetaGuess".format(parname)

        printBar(mark='-')
        print "\tthetaGuess:{}".format(thetaGuess)

        # SAY WHAT TODO:
        if ToDo is 'minimize_Chi2':
            print '\tMINIMAZING...'
            result = opt.minimize(self.updateChi2, thetaGuess, method=method)
        elif ToDo is 'first_marginalize_H0_then_minimize_Chi2':
            if 'H0' in self.model.freePars:
                print '\tMARGINALIZING \& MINIMIZING, BE PATIENT...'
                print 'Maginalizing H0 in the range: [{0},{1}]'\
                    .format(self.model.lower['H0'], self.model.upper['H0'])
                result = opt.minimize(self.updateMarginalizeH0, thetaGuess, method=method)
            else:
                sys.exit('H0 is not a Free parameter')
        else:
            sys.exit('Error: <opt> not implemented in getBestFit()')

        self.bestFit = result.x
        self.bestChi2 = result.fun
        self.updateConfidenceLevels()
        print self.bestFit[0]
        self.ax.plot([self.bestFit[0]], [self.bestFit[1]], 'ro')

        if verbose is True:
            printBar(mark='<>', nbars=1, size=25)
            print 'BEST FIT RESULT:'
            printBar(mark='<>', nbars=1, size=25)
            print result
            printBar(mark='--', nbars=1, size=25)
            print 'Maximum likelihood result: {}'.format(result.fun)
            print 'Best Fit = {0} (guess: {1})'.format(self.bestFit, thetaGuess)
            printBar(mark='<>', nbars=2, size=25)
            print ''

    def plotConfidenceContour(self):
        """docstring for fname"""
        om_points = np.linspace(0.01, 0.6, 40)
        ol_points = np.linspace(0.3, 1.2, 40)
        X = []
        Y = []
        Z = []
        for om in om_points:
            for ol in ol_points:
                # print om, ol
                X.append(om)
                Y.append(ol)
                Z.append(self.updateChi2_new(om, ol))
        xi = np.linspace(min(X), max(X), 2*len(X))
        yi = np.linspace(min(Y), max(Y), 2*len(Y))
        zi = ml.griddata(X, Y, Z, xi, yi, interp='linear')
        # zi = ml.griddata(X, Y, Z, xi, yi)
        # print zi
        # plt.pcolormesh(xi, yi, zi, cmap=plt.get_cmap('Blues'))
        # bar = plt.colorbar()
        # bar.set_label(r'$\chi^2$')
        # plt.contourf(xi, yi, zi,
        plt.contour(xi, yi, zi,
                    levels=(self.confidenceOneSigma,
                            self.confidenceTwoSigma,
                            self.confidenceThreeSigma),
                    linewidths=2, cmap=cm.Set2)
        #            linewidths=2, colors=('r', 'g', 'b'))
        # clabel(cset, inline=True, fmt='%1.1f', fontsize=10)

    def noBigBang(self, x):
        return 4.0 * x * (np.cosh(np.arccosh((1.0-x)/x)/3.0))**3.0

    def eternal(self, X):
        Y = []
        for x in X:
            if x < 1:
                y = 0.0
            else:
                y = 4.0 * x * (np.cos(np.arccos((1.0-x)/x)/3.0 + 4.0 * np.pi / 3.0))**3.0
            Y.append(y)
        return Y

    def addRegionsToPlot(self):
        """docstring for addRegionsToPlot"""
        line_Flat = Line2D([0.0, 2.5], [1.0, -1.5], c='b', ls='--')
        line_Acce = Line2D([0.0, 3.0], [0.0, 1.5], c='k', ls='-', fillstyle='bottom')
        line_q_neg05 = Line2D([0.0, 3.0], [0.0+0.5, 1.5+0.5], c='k', ls='--')
        line_q_pos05 = Line2D([0.0, 3.0], [0.0-0.5, 1.5-0.5], c='k', ls='--')
        # Add the lines to the plot
        self.ax.add_line(line_Flat)
        self.ax.add_line(line_Acce)
        self.ax.add_line(line_q_neg05)
        self.ax.add_line(line_q_pos05)

        x_acce = np.linspace(0.0, 2.5, 10)
        y_acce = x_acce/2.0

        x_noBB = np.linspace(0.00001, 0.5, 20)
        y_noBB = self.noBigBang(x_noBB)
        self.ax.plot(x_noBB, y_noBB, 'k-', linewidth=1.0)

        x_eternal = np.linspace(0.0, 2.5, 10)
        y_eternal = self.eternal(x_eternal)
        self.ax.plot(x_eternal, y_eternal, 'k-', linewidth=1.0)

        self.ax.fill_between(x_noBB, 2.0, y_noBB, facecolor='grey', alpha=0.5)
        self.ax.fill_between(x_acce, y_eternal, y_acce, facecolor='red', alpha=0.5)
        self.ax.fill_between(x_eternal, -0.5, y_eternal, facecolor='g', alpha=0.5)

    def getPlot(self):
        # self.ax.plot()
        self.plotConfidenceContour()
        self.addRegionsToPlot()
        self.ax.set_xlim(0.0, 2.5)
        self.ax.set_ylim(-0.5, 2.0)
        # plt.savefig('plots/SN_Union_Conf_Interv.png')
        # plt.show()
        return self.ax
