'''
Document this
Author: Efrain Torres
date: Jan 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import numpy as np
import os

_dataPath = os.path.dirname(os.path.realpath(__file__))


def load_SNdata(file_name, verbose=True):
    SNData = np.loadtxt(file_name,
                        dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
                               'formats': ('|S15', np.float, np.float, np.float, np.float)},
                        skiprows=5)
    if verbose is True:
        _printBar('-')
        print 'Number of SuperNova Loaded :{}'.format(len(SNData))
        _printBar('-')
    return SNData


def load_SNcovmat(file_name, verbose=True):
    covmat = np.loadtxt(file_name)
    if verbose is True:
        _printBar('-')
        print 'Covariance Matrix is a ({0},{1}) matrix'.format(len(covmat), len(covmat[0]))
        _printBar('-')
    return covmat


def get_unionSN21Data():
    return load_SNdata(file_name=_dataPath+'/SN_Union2_1.dat', verbose=True)


def get_unionSN21Covmat():
    return load_SNcovmat(file_name=_dataPath+'/SN_Union2_1_covmat.dat', verbose=True)


def _printBar(mark='*', nbars=1, size=30):
    """docstring for printBar"""
    for x in xrange(0, nbars):
        print mark[:6]*size


if __name__ == "__main__":
    import os
    print(os.path.dirname(os.path.realpath(__file__)))
