'''
Document this
Author: Efrain
date: Jan 2016

email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import numpy as np
from scipy.integrate import quad
import sys

parTex = {'OmegaM': r'$\Omega_M$',
          'OmegaR': r'$\Omega_R$',
          'OmegaL': r'$\Omega_\Lambda$',
          'OmegaK': r'$\Omega_K$',
          'H0': r'$H_0$',
          'ns': r'$n_s$',
          'Ds': r'$D_s$',
          'mu': r'$\mu(z)$'
          }


class CosmoModel(object):
    '''
    A FRW cosmological model
    pars: Dictionary with format
         par= {'par_name': [par_0, par_min, par_max, True/False]}
         where True/False defines if 'par_name' is a free parameter
    '''

    def __init__(self, pars={"OmegaM": [0.3, 0.0, 0.5, True],
                             "OmegaR": [0.0, 0.0, 0.0, False],
                             "OmegaL": [0.7, 0.5, 1.0, False],
                             "H0": [63.0, 50.0, 80.0, True]}):
        # This initializes:
        #                   self.value, self.lower, self.upper,
        #                   self.label, self.freePars,
        #                   self.nFreePars, self.plotLabel
        self.initPars(pars)

    def __str__(self):
        return self.out_info()

    def __call__(self, *args, **kwargs):
        "Displays some specific output"
        for d in sorted(dir(self)):
            print "{}: {}". format(d, str(getattr(self, d)))

    def out_info(self):
        class_name = self.__class__.__name__
        out = '\n=================================\n'
        out += 'This is {} with components:\n'.format(class_name)
        out += '\nFree Parameters: {}'.format(self.freePars)
        out += '\n=================================\n'
        return out

    def initPars(self, pars):
        self.value = {}
        self.lower = {}
        self.upper = {}
        self.label = {}
        self.nFreePars = 0
        self.freePars = {}
        self.plotLabel = ''
        for parname, parvals in pars.iteritems():
            self.checkPar(parname, parvals)
            self.setValue(parname, parvals)
            self.setRanges(parname, parvals)
            self.setLabel(parname, parvals)
            self.setFreePar(parname, parvals)
        self.setPlotLabel()

    def checkPar(self, parname, parvals):
        ''' Check if all pars are well defined'''
        bad = False
        if (parvals[1] > parvals[2] and parvals[-1] is True):
            err = '{0}_min > {0}_max'.format(parname)
            bad = True
        if (parvals[0] < parvals[1] and parvals[-1] is True):
            err = '{0}_0 < {0}_min'.format(parname)
            bad = True
        if (parvals[0] > parvals[2] and parvals[-1] is True):
            err = '{0}_0 > {0}_max'.format(parname)
            bad = True
        if bad is True:
            mes = 'ERROR: {} is bad defined:\nHINT: {}'.format(parname, err)
            sys.exit(mes)

    def setValue(self, parname, parvals):
        self.value[parname] = parvals[0]

    def setRanges(self, parname, parvals):
        self.lower[parname] = parvals[1]
        self.upper[parname] = parvals[2]

    def setLabel(self, parname, parvals):
        if parname in parTex:
            self.label[parname] = parTex[parname]
        else:
            self.label[parname] = parname

    def setFreePar(self, parname, parvals):
        if parvals[-1] is True:
            self.freePars[parname] = self.value[parname]
            self.nFreePars += 1

    def isFree(self, parname):
        free = False
        if parname in self.freePars:
            free = True
        return free

    def setPlotLabel(self):
        lnames = []
        lvalues = []
        for parname, label in self.label.iteritems():
            if parname != "OmegaR":
                # lnames += label + ','
                # lvalues += str(self.value[parname]) + ','
                lnames.append(label)
                lvalues.append(str(self.value[parname]))
        snames = ",".join(lnames)
        svalues = ",".join(lvalues)
        self.plotLabel = '[' + snames + ']=[' + svalues + ']'

        # for parname, label in self.label.iteritems():
        #     self.plotLabel += '{}={},'.format(label, self.value[parname])

        # self.plotLabel = r'$\Omega_M$={}, $\Omega_\Lambda$={}, $\Omega_K$={}'\
        #     .format(self.value['OmegaM'], self.value['OmegaL'], self.value['omegak'])

    def omegaKFromFriedmann(self):
        """Omega_K must obey the Friedmann Constraint  """
        return 1.0 - self.value['OmegaM'] - self.value['OmegaL'] - self.value['OmegaR']

    def Hubble(self, z):
        '''
        The dimensionless Hubble parameter E (z) = H(z)/H_0
        '''
        o_k = self.omegaKFromFriedmann()
        one_plus_z = 1.0 + z
        return np.sqrt(self.value['OmegaM']*(one_plus_z)**3.0 +
                       self.value['OmegaL'] +
                       self.value['OmegaR']*(one_plus_z)**4.0 +
                       o_k*(one_plus_z)**2.0)

    def horizonHubble(self, z):
        return 1.0/self.Hubble(z)

    def dLuminos(self, z):
        '''
        Luminosity distance
        '''
        c = 300000  # Speed Light
        o_k = self.omegaKFromFriedmann()

        I = quad(self.horizonHubble, 0.0, z)

        if o_k == 0.0:
            factor = c * (1.0 + z) / self.value['H0']
            disL = factor * I[0]
        elif o_k < 0.0:
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.value['H0'] * factor1)
            disL = factor * np.sin(factor1 * I[0])
        elif o_k > 0.0:
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.value['H0'] * factor1)
            disL = factor * np.sinh(factor1 * I[0])

        return disL

    def mu(self, z):
        return 5.0*np.log10(self.dLuminos(z)) + 25.0


class LCDM_Flat(CosmoModel):
    def __init__(self, OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 50.0, 80.0]):
        CosmoModel.__init__(self, pars={'OmegaM': [OmegaM[0], OmegaM[1], OmegaM[2], True],
                                        'OmegaR': [0.0, 0.0, 0.0, False],
                                        'OmegaL': [1.0-OmegaM[0], 0.0, 1.0, False],
                                        'H0': [H0[0], H0[1], H0[2], True]})
