
import numpy as np
# import os
# import matplotlib.pyplot as plt
from scipy.integrate import quad
import sys


class cosmoModel_New(object):
    '''
    A FRW cosmological model
    pars: Dictionary with format
         par= {'par_name': [par_0, par_min, par_max, 'parLabel', True/False]}
         where True/False defines if 'par_name' is a free parameter
    '''

    def __init__(self, pars={"OmegaM": [0.3, 0.0, 0.5, r'$\Omega_M$', True],
                             "OmegaR": [0.0, 0.0, 0.0, r'$\Omega_R$', False],
                             "OmegaL": [0.7, 0.5, 1.0, r'$\Omega_\Lambda$', False],
                             "OmegaK": [0.0, 0.0, 0.0, r'$\Omega_K$', False],
                             "H0": [63.0, 50.0, 80.0, '$\H_0$', True]}):
        self.pars = pars
        NFreePars = 0
        FreePars = {}
        parLabels = []
        bad = False
        for parname, parvals in self.pars.iteritems():
            # Check if all pars are well defined
            # print parname, parvals[0]
            if (parvals[1] > parvals[2] and parvals[-1] is True):
                err = '{0}_min > {0}_max'.format(parname)
                bad = True
            if (parvals[0] < parvals[1] and parvals[-1] is True):
                err = '{0}_0 < {0}_min'.format(parname)
                bad = True
            if (parvals[0] > parvals[2] and parvals[-1] is True):
                err = '{0}_0 > {0}_max'.format(parname)
                bad = True
            if bad is True:
                mes = 'ERROR: {} is bad defined:\nHINT: {}'.format(parname, err)
                sys.exit(mes)
            # Extract the Free pars
            if parvals[-1] is True:
                NFreePars += 1
                FreePars[parname] = parvals
                parLabels.append(parvals[3])

        self.NFreePars = NFreePars
        self.FreePars = FreePars
        self.parLabels = parLabels

    def __str__(self):
        return self.out_info()

    def __call__(self, *args, **kwargs):
        "Displays some specific output"
        for d in sorted(dir(self)):
            print "{}: {}". format(d, str(getattr(self, d)))

    def out_info(self):
        class_name = self.__class__.__name__
        out = '\n=================================\n'
        out += 'This is {} with components:\n'.format(class_name)
        # out += 'OmegaM: {}\nOmegaR: {}\nOmegaL: {}\nOmegaK: {}'.\
        #     format(self.OmegaM, self.OmegaR, self.OmegaL, self.OmegaK)
        out += '\nFree Parameters: {}'.format(self.FreePars)
        out += '\n=================================\n'
        return out

    def Hubble(self, z):
        '''
        The dimensionless Hubble parameter E (z) = H(z)/H_0
        '''
        o_r = self.pars['OmegaR'][0]
        o_m = self.pars['OmegaM'][0]
        o_l = self.pars['OmegaL'][0]
        # o_k = self.pars['OmegaK'][0]
        o_k = 1.0 - o_m - o_l - o_r
        return np.sqrt(o_m*(1.0+z)**3.0 + o_l + o_r*(1.0+z)**4.0 + o_k*(1.0+z)**2.0)

    def horizonHubble(self, z):
        return 1.0/self.Hubble(z)

    def dLuminos(self, z):
        '''
        Luminosty distance
        '''
        c = 300000  # Speed Light

        I = quad(self.horizonHubble, 0.0, z)

        if self.pars['OmegaK'][0] == 0.0:
            factor = c * (1.0 + z) / self.pars['H0'][0]
            disL = factor * I[0]
        elif self.pars['OmegaK'][0] < 0.0:
            o_k = 1.0 - self.pars['OmegaM'][0] - self.pars['OmegaL'][0] - self.pars['OmegaR'][0]
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.H0 * factor1)
            disL = factor * np.sin(factor1 * I[0])
        elif self.pars['OmegaK'][0] > 0.0:
            o_k = 1.0 - self.pars['OmegaM'][0] - self.pars['OmegaL'][0] - self.pars['OmegaR'][0]
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.H0 * factor1)
            disL = factor * np.sinh(factor1 * I[0])

        return disL

    def mu(self, z):
        return 5.0*np.log10(self.dLuminos(z)) + 25.0


class cosmoModel(object):
    '''
    A FRW cosmological model
    def __init__(self, omegas=[0.3, 0.0, 0.7, 0.0], H0=63.0, NFreePars=5, FreePars=['OmegaM', 'OmegaR', 'OmegaL', 'OmegaK', 'H0']):
    '''
    def __init__(self, omegas={"OmegaM": 0.3, "OmegaR": 0.0, "OmegaL": 0.7, "Omegak": 0.0}, H0=63.0, NFreePars=5, FreePars=['OmegaM', 'OmegaR', 'OmegaL', 'OmegaK', 'H0']):
        # if omegas is None:
        #     self.OmegaM = 0.3
        #     self.OmegaR = 0.0
        #     self.OmegaL = 0.7
        #     self.OmegaK = 0.0
        #     self.omegas = [self.OmegaM, self.OmegaR, self.OmegaL, self.OmegaK]
        # else:
        self.omegas = omegas
        self.OmegaM = omegas.get('OmegaM')
        self.OmegaR = omegas.get('OmegaR')
        self.OmegaL = omegas['OmegaL']
        self.OmegaK = omegas['OmegaK']
        # self.omegas = [self.OmegaM, self.OmegaR, self.OmegaL, self.OmegaK]
        self.H0 = H0
        self.pars = self.omegas
        self.pars['H0'] = self.H0
        self.NFreePars = NFreePars
        self.FreePars = {}
        for key, value in self.pars.iteritems():
            if key in FreePars:
                self.FreePars[key] = value

        self.label = r'$\Omega$={}, $\Omega_\Lambda$={}, $\Omega_k$={}'\
            .format(self.OmegaM, self.OmegaL, self.OmegaK)

    def __str__(self):
        return self.out_info()

    # def __call__(self, *args, **kwargs):
    #     "Displays some specific output"
    #     for d in sorted(dir(self)):
    #         print "{}: {}". format(d, str(getattr(self, d)))

    def out_info(self):
        class_name = self.__class__.__name__
        out = '\n=================================\n'
        out += 'This is {} with components:\n'.format(class_name)
        out += 'OmegaM: {}\nOmegaR: {}\nOmegaL: {}\nOmegaK: {}'.\
            format(self.OmegaM, self.OmegaR, self.OmegaL, self.OmegaK)
        out += '\nFree Parameters: {}'.format(self.FreePars)
        out += '\n=================================\n'
        return out

    def Hubble(self, z):
        '''
        The dimensionless Hubble parameter E (z) = H(z)/H_0
        '''
        o_r = self.OmegaR
        o_m = self.OmegaM
        o_l = self.OmegaL
        # o_k = self.OmegaK
        o_k = 1.0 - o_m - o_l - o_r
        return np.sqrt(o_m*(1.0+z)**3.0 + o_l + o_r*(1.0+z)**4.0 + o_k*(1.0+z)**2.0)

    def horizonHubble(self, z):
        return 1.0/self.Hubble(z)

    def dLuminos(self, z):
        '''
        Luminosty distance
        '''
        c = 300000  # Speed Light

        I = quad(self.horizonHubble, 0.0, z)

        if self.OmegaK == 0.0:
            factor = c * (1.0 + z) / self.H0
            disL = factor * I[0]
        elif self.OmegaK < 0.0:
            o_k = 1.0 - self.OmegaM - self.OmegaL - self.OmegaR
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.H0 * factor1)
            disL = factor * np.sin(factor1 * I[0])
        elif self.OmegaK > 0.0:
            o_k = 1.0 - self.OmegaM - self.OmegaL - self.OmegaR
            factor1 = np.sqrt(np.abs(o_k))
            factor = c * (1.0 + z) / (self.H0 * factor1)
            disL = factor * np.sinh(factor1 * I[0])

        return disL

    def mu(self, z):
        return 5.0*np.log10(self.dLuminos(z)) + 25.0

    # def SNChiSqrd(self, SNData, version='naive'):
    #     """docstring for chisqrd"""
    #     zData = SNData['RedShift']
    #     muData = SNData['Distance']
    #     muError = SNData['DistanceErr']
    #     if version is 'naive':
    #         chisqrd = 0.
    #         for z, mu, muerr in zip(zData, muData, muError):
    #             chisqrd += ((self.mu(z) - mu) / muerr)**2.0
    #     return chisqrd


class LCDM_Flat_New(cosmoModel_New):
    def __init__(self, OmegaM=[0.3, 0.0, 1.0], H0=[63.0, 50.0, 80.0]):
        cosmoModel_New.__init__(self, pars={'OmegaM': [OmegaM[0], OmegaM[1], OmegaM[2], r'$\Omega_M$', True],
                                            'OmegaR': [0.0, 0.0, 0.0, r'$\Omega_R$', False],
                                            'OmegaL': [1.0-OmegaM[0], 0.0, 1.0, r'$\Omega_L$', False],
                                            'OmegaK': [0.0, 0.0, 0.0, r'$\Omega_K$', False],
                                            'H0': [H0[0], H0[1], H0[2], r'$H_0$', True]})


class LCDM_Flat(cosmoModel):
    def __init__(self, OmegaM=0.3, H0=63.0):
        cosmoModel.__init__(self, omegas={'OmegaM': OmegaM, 'OmegaR': 0.0, 'OmegaL': 1.0-OmegaM, 'OmegaK': 0.0},
                            H0=H0, NFreePars=2, FreePars=['OmegaM', 'H0'])

if __name__ == "__main__":

    import os
    import matplotlib.pyplot as plt
    import time

    data_dir = os.path.join('../data')
    data_file = 'SN_Union2_1.dat'
    plot_dest = os.path.join('../plots')
    plot_name = 'SuperNova.png'

    font = {'family': 'Times New Roman',
            'weight': 'normal',  # light, bold
            'size': 15}
    plt.rc('font', **font)  # change general fonts

    def Timing(theFunction, **kwargs):
        timeStart = time.time()
        theFunction(**kwargs)
        timeFinish = time.time()
        print 'Time elapsed:{}'.format(timeFinish - timeStart)

    def plotHubbleDiagram(ax, dataSN):
        '''
        dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
        '''
        redShift = dataSN['RedShift']
        distance = dataSN['Distance']
        distanceErr = dataSN['DistanceErr']

        ax.errorbar(redShift, distance, yerr=distanceErr, fmt='o', ms=3)

        ax.set_xlabel(r'$z$')
        ax.set_ylabel(r'$\mu(z)$')

    def plotmu(ax, model, zarray, H0):
        """docstring for plotmu"""
        muarray = []
        for z in zarray:
            muarray.append(model.mu(z, H0))
        ax.plot(zarray, muarray, label=model.label)

    def main():
        """docstring for main"""

        fig1 = plt.figure()
        ax1 = fig1.add_subplot(111)

        H0 = 71.0
        zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = np.loadtxt(os.path.join(data_dir, data_file),
                                   dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
                                          'formats': ('|S15', np.float, np.float, np.float, np.float)},
                                   skiprows=5)
        plotHubbleDiagram(ax1, UnionSN21Data)

        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))

        # self.omegas = [OmegaM, OmegaR, OmegaL, OmegaK]
        model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0])
        model2 = cosmoModel(omegas=[0.2, 0.0, 0.0, 0.8])
        model3 = cosmoModel(omegas=[1.0, 0.0, 0.0, 0.0])

        models = [model1, model2, model3]

        for model in models:
            print model
            plotmu(ax1, model, zarray, H0)

        plt.legend(loc='lower right')

        plt.savefig(os.path.join(plot_dest, plot_name), dpi=90, bbox_inches='tight')
        plt.show()

    def testchisqrd():
        """docstring for testchisqrd"""
        # fig1 = plt.figure()
        # ax1 = fig1.add_subplot(111)

        H0 = 71.0
        # zarray = np.linspace(0.01, 1.7, 50)

        # http://supernova.lbl.gov/union/figures/SCPUnion2.1_mu_vs_z.txt
        UnionSN21Data = np.loadtxt(os.path.join(data_dir, data_file),
                                   dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
                                          'formats': ('|S15', np.float, np.float, np.float, np.float)},
                                   skiprows=5)
        print 'Number of SuperNova:{}'.format(len(UnionSN21Data))
        # plotHubbleDiagram(ax1, UnionSN21Data)

        model1 = cosmoModel(omegas=[0.27, 0.0, 1.0 - 0.27 - 0.0, 0.0])
        # plt.legend(loc='lower right')
        # plt.show()
        print model1
        print model1.SNChiSqrd(UnionSN21Data, H0)

    # main()

    # testchisqrd()

    Timing(testchisqrd)
