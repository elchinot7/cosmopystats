'''
Document this
Author: Efrain Torres
date: Jan 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''
import numpy as np
from .utils import printBar


def load_SNdata(file_name, verbose=True):
    SNData = np.loadtxt(file_name,
                        dtype={'names': ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
                               'formats': ('|S15', np.float, np.float, np.float, np.float)},
                        skiprows=5)
    if verbose is True:
        printBar('-')
        print 'Number of SuperNova Loaded :{}'.format(len(SNData))
        printBar('-')
    return SNData


def load_SNcovmat(file_name, verbose=True):
    covmat = np.loadtxt(file_name)
    if verbose is True:
        printBar('-')
        print 'Covariance Matrix is a ({0},{1}) matrix'.format(len(covmat), len(covmat[0]))
        printBar('-')
    return covmat
