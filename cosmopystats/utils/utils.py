# -------------------------------------------------
# Some generic functions to be imported from elsewere
# Author: Efrain Torres
# date: Jan 2016
# Info: import this functions with:
#       from myutils.myUtils import *
# -------------------------------------------------
# import time
from __future__ import division
import os
import socket
from datetime import datetime


def setup_text_plots(fontsize=8, family='serif', style='normal', weight='normal', usetex=True):
    """
    This function adjusts matplotlib settings so that all figures in the
    textbook have a uniform format and look.
    """
    import matplotlib
    matplotlib.rc('legend', fontsize=fontsize, handlelength=3)
    matplotlib.rc('axes', titlesize=fontsize)
    matplotlib.rc('axes', labelsize=fontsize)
    matplotlib.rc('xtick', labelsize=fontsize)
    matplotlib.rc('ytick', labelsize=fontsize)
    matplotlib.rc('text', usetex=usetex)
    matplotlib.rc('font', size=fontsize, family=family,
                  style=style, variant='normal',
                  stretch='normal', weight=weight)


def Timing(theFunction, funcName=None, **kwargs):
    import time
    timeStart = time.time()
    theFunction(**kwargs)
    timeFinish = time.time()
    if funcName is not None:
        printBar(size=45)
        print ' {} -> Time elapsed: {:.2f} sec'\
            .format(funcName, timeFinish - timeStart)
        printBar(size=45)
    else:
        printBar(size=40)
        print '\tTime elapsed: {:.2f} sec'\
            .format(timeFinish - timeStart)
        printBar(size=40)


def printBar(mark='*', nbars=1, size=30):
    """docstring for printBar"""
    for x in xrange(0, nbars):
        print mark[:6]*size


def progressbar_old(advance):
    import sys
    sys.stdout.write('\r')
    sys.stdout.write("[%-20s] %d%%" % ('='*int(advance/5.), advance))
    sys.stdout.flush()


def progressbar(advance, total, mark='=', bar_length=None):
    """
    Print Progress Bar in terminal:

    ARGS:
        advance (int): Typically is an ``loop for index``.
        total (int):   The total number of steps in the ``loop for``
        mark (str):    Is the mark used inside the progress bar.
                       Default is '='
        bar_lenght (float): Is the window-ratio to be filled by the
                            progress bar.
                            0 < bar_length < 1

    RETURN:
        Progress bar printed in terminal.

        [===       ] 30%

    """
    import sys

    terminal_width = _get_terminal_width()

    if bar_length is None:
        bar_width = int((2/3)*terminal_width)
    else:
        bar_width = int(bar_length*terminal_width)

    sys.stdout.write('\r')
    sys.stdout.write("[\033[94m{mark:{fill}{width}}\033[0m] {adv} %".format(mark=mark*int((bar_width - 1)*(advance)/total) + ">",
                                                                            fill='', width=bar_width, adv=int(100*advance/total)))
    sys.stdout.flush()
    if advance == total:
        print ' DONE!\n'


def print_sign(fig, posx=0.6, posy=0.01):
    """
    SIGN to be printed in fig
    """
    AUTHOR = os.getlogin()
    MACHINE = socket.gethostname()
    # MACHINE = os.getenv('HOST')
    SIGN = 'Plots by ' + AUTHOR + '@' + str(MACHINE) + ' - '
    NOW = datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S')
    fig.text(posx, posy, SIGN + NOW, color='b', family='monospace', fontsize=7)


def _get_terminal_width():
    """
    Get the terminal width dinamically.

    ARGS: None

    RETURN:
        width (int): Returns the result ``tput cold``

    """
    import subprocess

    command = ['tput', 'cols']

    try:
        width = int(subprocess.check_output(command))
    except OSError as e:
        print("Invalid Command '{0}': exit status ({1})".format(
              command[0], e.errno))
    except subprocess.CalledProcessError as e:
        print("Command '{0}' returned non-zero exit status: ({1})".format(
              command, e.returncode))
    else:
        return width


def main():
    import time
    for i in xrange(10):
        time.sleep(1)
        progressbar(advance=i+1, total=10, mark="*")


if __name__ == "__main__":
    main()
