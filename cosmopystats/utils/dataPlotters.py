'''
Document this
Author: Efrain
date: Jan 2016
email: efrain@fisica.ugto,mx
       efrain@ifm.umich.mx
'''


def plotHubbleDiagram(ax, dataSN):
    '''
    dataSN: has the format: ['SN_Name', 'RedShift', 'Distance', 'DistanceErr', 'Prob'],
    '''
    redShift = dataSN['RedShift']
    distance = dataSN['Distance']
    distanceErr = dataSN['DistanceErr']

    ax.errorbar(redShift, distance, yerr=distanceErr, fmt='o', ms=3)

    ax.set_xlabel(r'$z$')
    ax.set_ylabel(r'$\mu(z)$')


def plotmu(ax, model, zarray):
    """docstring for plotmu"""
    muarray = []
    for z in zarray:
        muarray.append(model.mu(z))
    ax.plot(zarray, muarray, label=model.plotLabel)
